import zmq
import json
import signal
import time
import datetime


def to_bytes(x):
    if isinstance(x, dict):
        return json.dumps(x).encode('utf-8')
    if isinstance(x, str):
        return x.encode('utf-8')
    return x


def send_multipart(socket: zmq.Socket, *parts):
    socket.send_multipart([to_bytes(part) for part in parts])


signal.signal(signal.SIGINT, signal.SIG_DFL)  # Ctrl+c enable

respJson = {
    "Status": 0,
    "Result": {
        "ResultData": {
            "Message": "Success"
        }
    }
}

# ========================
# config
# ========================
ai_name = 'test_ai'
pub_port = 15001
sub_port = 15002
ip_addr = 'tcp://127.0.0.1'

# ========================
# socket
# ========================
context = zmq.Context()
sub = context.socket(zmq.SUB)
sub.connect(f'{ip_addr}:{sub_port}')
pub = context.socket(zmq.PUB)
pub.connect(f'{ip_addr}:{pub_port}')

# receive only message
print(f'receiveFilter = [{ai_name}]')
sub.setsockopt(zmq.SUBSCRIBE, ai_name.encode('utf-8'))

# ========================
# message receive loop
# ========================
while True:
    # receive message
    [message0, message1, message2] = sub.recv_multipart()

    # message1 json
    topic = message0.decode('utf-8')
    rcvMsgJson = json.loads(message1)
    metadata: dict = rcvMsgJson['metadata']

    # ========================
    # recv data print
    # ========================
    print(f'recv (topic {topic}) message: {message1.decode("utf-8")}')
    print(f'recv bin data size(byte): {len(message2)}')

    # ========================
    # resp data set
    # ========================
    # start time
    dt_now = datetime.datetime.now()
    respJson['Start'] = dt_now.strftime('%Y/%m/%d %H:%M:%S.%f')[:-3]

    # do something....
    time.sleep(metadata.get('delay') or 0)

    # end time
    dt_now = datetime.datetime.now()
    respJson['End'] = dt_now.strftime('%Y/%m/%d %H:%M:%S.%f')[:-3]

    topic = 'Main'
    print(f'send (topic {topic}) message: {message1.decode("utf-8")} data: {json.dumps(respJson)}')

    # resp data send
    send_multipart(pub, topic, message1, respJson)
