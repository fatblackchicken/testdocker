FROM python:3.6.6-slim

#ENV http_proxy="http://<user>:<password>@<proxyurl>:<proxyport>"
#ENV https_proxy="http://<user>:<password>@<proxyurl>:<proxyport>"

WORKDIR /home

RUN mkdir /home/config /home/data

RUN apt-get update

RUN apt-get install -y build-essential python3-dev libzmq3-dev

RUN pip3 install pyzmq --install-option="--zmq=bundled"

COPY aimq.py /home/.
CMD python aimq.py
